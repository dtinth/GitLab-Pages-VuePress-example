module.exports = {
  title: 'Hello VuePress',
  description:
    'An example project that deploys a VuePress site to GitLab Pages using GitLab CI',
  base: '/GitLab-Pages-VuePress-example/',
  dest: 'public'
}
