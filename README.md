---
home: true
actionText: View source code →
actionLink: https://GitLab.com/dtinth/GitLab-Pages-VuePress-example
features:
  - title: Simplicity First
    details:
      Minimal setup with markdown-centered project structure helps you focus on
      writing.
  - title: Vue-Powered
    details:
      Enjoy the dev experience of Vue + webpack, use Vue components in markdown,
      and develop custom themes with Vue.
  - title: Performant
    details:
      VuePress generates pre-rendered static HTML for each page, and runs as an
      SPA once a page is loaded.
---

# GitLab-Pages-VuePress-example

This [repository](https://GitLab.com/dtinth/GitLab-Pages-VuePress-example)
contains an example of deploying a Vuepress site to GitLab Pages using GitLab CI.
[View result](https://dtinth.gitlab.io/GitLab-Pages-VuePress-example/).
